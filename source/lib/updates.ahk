w9x := A_OSType="WIN32_NT" ? FALSE : TRUE
isL := A_AhkVersion > "1.0.48.05" ? TRUE : FALSE
Ptr := A_IsUnicode ? "Ptr" : "UInt"
PtrP := A_IsUnicode ? "PtrP" : "UIntP"
AStr := A_IsUnicode ? "AStr" : "Str"
WStr := A_IsUnicode ? "WStr" : "UInt"	; pointer to a buffer containing Unicode-converted string
i := "A_PtrSize"
if !A_PtrSize
	%i% := (A_PtrSize ? A_PtrSize : "4")
AW := A_IsUnicode ? "W" : "A"
/*
msgbox,
(
w9x=%w9x%
isL=%isL% (version %A_AhkVersion%)
Ptr=%Ptr%
PtrP=%PtrP%
AStr=%AStr%
WStr=%WStr%
A_PtrSize=%A_PtrSize%
AW=%AW%
)
*/
