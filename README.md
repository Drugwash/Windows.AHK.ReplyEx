# ReplyEx

<center> ![button ReplyEx 1.0.1.0](ss_replyex1010.png) </center>

Creates reply-to-reply URL for WordPress blogs.

Some blogs and sites based on WordPress, where comments are allowed may have a shallow limit for nested comments. As such one cannot properly reply to the previous comment if needed, at some point.

This small helper tool aims to improve the situation by automatically converting the URL of the **previous** comment _(the one that the user wants to reply to)_ into a usable reply-URL. That URL is placed in the clipboard, and the user only has to paste it in the address bar and go. It may open another tab or window - depending on browser settings - but the conversation will keep its intended flow despite the lack of further indentation.

Please be aware that certain WordPress themes may not provide the comment URLs, in which case this tool cannot be used. Those URLs are usually found under the date/time of the comment, and most often they can be distinguished as clickable URLs.

There are two distinct formats for the reply URL: one uses **?** and the other uses **&**. Before proceeding to create the reply URL the user should first hover any of the **Reply** buttons/links on the page and notice in the browser statusbar (does it have one?!) whether the URLs use either of the aforementioned character towards their end. Then use the appropriate approach for creating the URL - that is _click_ (for **?**) or _Ctrl+click_ (for **&**) on the blue button.        
See the examples below:     
![](example_reply_qm.png)		

![](example_reply_as.png)

If the URL of the previous comment is clickable then:

- [Ctrl+]click the blue button in this floating window - it turns red
- right-click the previous comment's date/time URL and select **Copy URL** or **Copy link** - or whatever your browser chose to name that function as
- paste in Address Bar and Go

Floating window management:

- Drag colored border with the mouse to position on screen      
- Right-click to hide floating window to tray       
- Double-click its tray icon to show

Short version of usage (only on WordPress blogs and sites):

- click the blue button (turns red) [**?** mode] OR     
- Ctrl+click the blue button (turns red) [**&** mode];      
- copy the previous reply URL to clipboard (usually it's under post date & time)       
- paste in Address Bar and Go

Enjoy!

**© Drugwash, 2012-2023**
